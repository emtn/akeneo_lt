import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Vuelidate from 'vuelidate'
import VueCookies from 'vue-cookies'

import TermsAndCondition from "./components/TermsAndCondition";
import mainpage from "./components/mainpage";

Vue.config.productionTip = false;


Vue.use(Vuelidate);
Vue.use(VueRouter);
Vue.use(VueCookies);

Vue.$cookies.config('30d');
Vue.$cookies.get("cookie");

const routes = [
  { path: '/', component: mainpage},
  { path: '/privatumo_politika', component: TermsAndCondition}
]

const router = new VueRouter({
  mode:'history',
  scrollBehavior(to, from, savedPosition){
    if (to.hash) {
      return { selector: to.hash}
    } else if (savedPosition) {
      return savedPosition;
    } else {
      return {x:0, y:0}
    }
  },
  routes
});



new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
