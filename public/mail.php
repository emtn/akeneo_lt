<?php
if($_POST) {
    $raw = file_get_contents('php://input');
    $data = json_decode($raw);
    if ($data->email && $data->name && $data->phone) {
        $to_email = 'hello@emotion.lt';
        $subject = "Akeneo.lt";
        $body = "Mane domina Akeneo. " . "<br>Vardas: " . $data->name . "<br>Telefonas: " . $data->phone . "<br>El. paštas: " . $data->email;
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=utf-8';
        $headers[] = "From: akeneo@emotion.lt";
        try {
            if (mail($to_email, $subject, $body, $headers)) {
                $success = 1;
            } else {
                $success = 0;
            };
        } catch (\Exception $exception) {
            $success = 0;
        }
    }
    echo json_encode(['success' => $success]);
} else {
    echo json_encode(['success' => 0]);
}